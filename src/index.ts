import { MongoClient } from 'mongodb'
import { config } from 'dotenv'
import { log } from './utils'

void (async () => {
    config()
    const client = new MongoClient(process.env.MONGO_URI!)
    await client.connect()
    client.close()
})()
