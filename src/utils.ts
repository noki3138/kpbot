import fecha from 'fecha'
import winston from 'winston'
import colors from 'colors'

const log = winston.createLogger({
    transports: [
        new winston.transports.Console({
            format: winston.format.printf(({ level, message }) => {
                const ts = fecha.format(new Date(), 'DD.MM.YYYY hh:mm:ss')
                level = `[${level.toUpperCase()}]`.padEnd(7, ' ')
                switch (level) {
                    case '[WARN] ':
                        level = colors.yellow(level)
                        break
                    case '[ERROR]':
                        level = colors.red(level)
                        break
                    default:
                        level = colors.bold(level)
                        break
                }
                return `${colors.blue(ts)} ${level} ${message}`
            })
        }),
        new winston.transports.File({
            dirname: 'logs',
            filename: 'debug.log',
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()
            )
        }),
        new winston.transports.File({
            dirname: 'logs',
            filename: 'error.log',
            format: winston.format.combine(
                winston.format.timestamp(),
                winston.format.json()
            ),
            level: 'error'
        })
    ],
})

export { log }
